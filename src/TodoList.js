import React, { useState } from 'react';
import './TodoList.css';

const TodoList = () => {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState('');
  const [warning, setWarning] = useState('');
  const [isChecked, setIsChecked] = useState({
    home: false,
    work: false,
    personal: false
  });

  const handleAddTodo = () => {
    if (!newTodo) return;
  
    const labels = Object.keys(isChecked).filter(label => isChecked[label]);
    const todoWithLabels = labels.map(label => `${newTodo} (${label})`);
  
    if (todoWithLabels.some(todo => todos.includes(todo))) {
      setWarning('Warning: Duplicate todo exists!');
    } else {
      setTodos([...todos, ...todoWithLabels]);
      setNewTodo('');
      setWarning('');
    }
  }
  

  const handleCheckboxChange = (label) => {
    setIsChecked(prevState => ({
      ...prevState,
      [label]: !prevState[label]
    }));
  }

  return (
    <div className='container'>
      <h1>Todo List</h1>
      {warning && <p className='warning'>{warning}</p>}
      <div>
        <input
          type="text"
          value={newTodo}
          onChange={(e) => setNewTodo(e.target.value)}
          className='todoInput'
        />
      </div>
      <div className='checkbox'>
        <label>
          <input
            type="checkbox"
            checked={isChecked.home}
            onChange={() => handleCheckboxChange('home')}
          />
          Home
        </label>
        <label>
          <input
            type="checkbox"
            checked={isChecked.work}
            onChange={() => handleCheckboxChange('work')}
          />
          Work
        </label>
        <label>
          <input
            type="checkbox"
            checked={isChecked.personal}
            onChange={() => handleCheckboxChange('personal')}
          />
          Personal
        </label>
      </div>
      <button className='button' onClick={handleAddTodo}>Add Todo</button>
      <ul>
        {todos.map((todo, i) => <li key={i}>{todo}</li>)}
      </ul>
    </div>
  );
}

export default TodoList;
